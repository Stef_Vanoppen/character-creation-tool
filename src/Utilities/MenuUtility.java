package Utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Properties;

public abstract class MenuUtility {

    public static int textLag;

    private static final int FULL_WIDTH = 32;

    public static String thickLine(){
        return generateCharNTimes('*', FULL_WIDTH);
    }

    public static String thinLine(){
        return generateCharNTimes('-', FULL_WIDTH);
    }

    private static String generateCharNTimes(char c, int amount) {
        StringBuilder text = new StringBuilder();
        for (int i = 0; i < amount; i++) {
            text.append(c);
        }
        return text.toString();
    }

    public static String center(String text) {
        return String.format("%" + (FULL_WIDTH/2 + text.length()/2) + "s", text);
    }

  /**  public static void iniTextLag(){
        Path pathToIni = Path.of(String.valueOf(SaveGame.fullPathToSave.resolve("rpgtest.properties")));
        try (FileInputStream inputStream = new FileInputStream(String.valueOf(pathToIni));){
            Properties properties = new Properties();
            properties.load(inputStream);
            textLag = Integer.parseInt(properties.getProperty("textlag"));
        } catch (IOException e) {
            System.out.println("rpgtest.properties not found. Using fallback value!");
            textLag = 20;
        }

    } **/


    public static void slothPrint(String string){
        System.out.println("");
        Thread bred = new Thread(() -> {
            for (int i = 0; i < string.length(); i++) {
                try {
                    System.out.print(string.charAt(i));
                    Thread.sleep(textLag);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        bred.start();
        try {
            bred.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
