package PersonStuff;

import java.io.Serializable;
import java.util.Objects;
import java.util.StringJoiner;


import enums.*;

public class Person implements Comparable<Person>, Serializable {


    private String voornaam;
    private String achternaam;
    private Gender gender;
    private int age;
    private float gewicht;
    private float lengte;
    private Race race;
    private RaceUncommon raceUncommon;
    private Profession profession;
    private HumanRace humanRace;

    //maybe add the subrace of human things over here ?

    public Person(String voornaam, String achternaam, Gender gender, Race race, Profession profession) {
        this.voornaam = voornaam;
        this.achternaam = achternaam;
        this.gender = gender;
        this.race = race;
        this.profession=profession;

    }


    public Person(String voornaam, String achternaam, Gender gender, RaceUncommon raceUncommon,Profession profession) {
        this.voornaam = voornaam;
        this.achternaam = achternaam;
        this.gender = gender;
        this.raceUncommon = raceUncommon;
        this.profession = profession;

    }


    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }


// learned how to use stringjoiner

    @Override
    public String toString() {


        final var joiner = new StringJoiner(", ", "" + "", "");
        if (race != null) {
            joiner.add(voornaam + " " + achternaam + " " + gender + " " + race + " " + profession );
        }
        if (raceUncommon != null) {
            joiner.add(voornaam + " " + achternaam + " " + gender + " " + raceUncommon + " " + profession);
        }
        return joiner.toString();
    }


    @Override
    public int hashCode() {
        return Objects.hash(voornaam, achternaam, gender);
    }


    @Override
    public int compareTo(Person o) {
        return 0;
    }
}