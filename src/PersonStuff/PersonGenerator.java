package PersonStuff;

import enums.Gender;
import enums.HumanRace;
import enums.Profession;
import enums.Race;
import util.KeyboardUtility;

import java.util.Collection;
import java.util.HashSet;
import java.util.Random;

public final class PersonGenerator {
    private final static Random RAND_NUM_GEN = new Random();
    private static HumanRace humanRace;

    public static Person generate() {
        Gender gender = genGender();
        Race race = genRace();
        humanRace = genHuman();
        Profession profession = genProfessions();
        String voornaam = genVoornaam(gender, race);
        String achternaam = genAchternaam(race); //verwijzing naar de lists
        return new Person(voornaam, achternaam, gender, race, profession);
    }

    public static Person generateSpecific(){
    Gender gender = KeyboardUtility.askForChoice("Choose your gender",Gender.values());
    Race race = KeyboardUtility.askForChoice("Choose your race",Race.values());
    if (race == Race.HUMAN){
        humanRace = KeyboardUtility.askForChoice("Choose your origin",HumanRace.values());
    }
    Profession profession = KeyboardUtility.askForChoice("Choose a profession !",Profession.values());
        String voornaam = genVoornaam(gender, race);
        String achternaam = genAchternaam(race); //verwijzing naar de lists
        return new Person(voornaam, achternaam, gender, race, profession);
    }

    public static Person generateHuman() {
        Gender gender = genGender();
        Race race = Race.HUMAN;
        humanRace = genHuman();
        Profession profession = genProfessions();
        String voornaam = genVoornaam(gender, race);
        String achternaam = genAchternaam(race); //verwijzing naar de lists
        return new Person(voornaam, achternaam, gender, race, profession);
    }
    public static Person generateHumanMale() {
        Gender gender = Gender.MALE;
        Race race = Race.HUMAN;
        humanRace = genHuman();
        Profession profession = genProfessions();
        String voornaam = genVoornaam(gender, race);
        String achternaam = genAchternaam(race); //verwijzing naar de lists
        return new Person(voornaam, achternaam, gender, race, profession);
    }

    public static Person generateHumanFemale() {
        Gender gender = Gender.FEMALE;
        Race race = Race.HUMAN;
        humanRace = genHuman();
        Profession profession = genProfessions();
        String voornaam = genVoornaam(gender, race);
        String achternaam = genAchternaam(race); //verwijzing naar de lists
        return new Person(voornaam, achternaam, gender, race, profession);
    }

    public static Person generateHumanFemaleGermanic() {
        Gender gender = Gender.FEMALE;
        Race race = Race.HUMAN;
        humanRace = HumanRace.Midlander_Germanic;
        Profession profession = genProfessions();
        String voornaam = genVoornaam(gender, race);
        String achternaam = genAchternaam(race); //verwijzing naar de lists
        return new Person(voornaam, achternaam, gender, race, profession);
    }

    public static Person generateHumanMaleGermanic() {
        Gender gender = Gender.MALE;
        Race race = Race.HUMAN;
        humanRace = HumanRace.Midlander_Germanic;
        Profession profession = genProfessions();
        String voornaam = genVoornaam(gender, race);
        String achternaam = genAchternaam(race); //verwijzing naar de lists
        return new Person(voornaam, achternaam, gender, race, profession);
    }

    public static Person generateElf() {
        Gender gender = genGender();
        Race race = Race.ELF;
        //humanRace = genHuman();
        Profession profession = genProfessions();
        String voornaam = genVoornaam(gender, race);
        String achternaam = genAchternaam(race); //verwijzing naar de lists
        return new Person(voornaam, achternaam, gender, race, profession);
    }

    public static Person generateDwarf() {
        Gender gender = genGender();
        Race race = Race.DWARF;
        //humanRace = genHuman();
        Profession profession = genProfessions();
        String voornaam = genVoornaam(gender, race);
        String achternaam = genAchternaam(race); //verwijzing naar de lists
        return new Person(voornaam, achternaam, gender, race, profession);
    }

    public static Person generateHalfling() {
        Gender gender = genGender();
        Race race = Race.HALFLING;
        //humanRace = genHuman();
        Profession profession = genProfessions();
        String voornaam = genVoornaam(gender, race);
        String achternaam = genAchternaam(race); //verwijzing naar de lists
        return new Person(voornaam, achternaam, gender, race, profession);
    }


    //order of importance
    //Race - gender - profession

    //create different generators within the persongenerator that use their own enums or find out how to generate a
    //a random from specific choices from the array.

    private static Profession genProfessions() {
        return (Profession)
                randElementFromArray(Profession.values());
    }


    private static Race genRace() {
        return (Race) randElementFromArray(Race.values());

    }
    private static HumanRace genHuman(){

        return (HumanRace) randElementFromArray(HumanRace.values());

    }


    public static Collection<Person> generatePeople(int amount) {
        Collection<Person> people = new HashSet<>(); //swapped to hashset
        for (int i = 0; i < amount; i++) {
            people.add(generate());
        }
        return people;
    }


    private static String genAchternaam(Race race) {
        String[] achternaamList;
        if (race == Race.DWARF) {
            achternaamList = geefAchternaamListDwarf();
        } else if (race == Race.ELF) {
            achternaamList = geefAchternaamListElf();
        } else if (race == Race.HUMAN) {
            if(humanRace == HumanRace.Celtic){
                achternaamList = geefAchternaamListCelticHuman();
            }
            else if (humanRace == HumanRace.Northern){
                achternaamList = geefAchternaamListNorthernHuman();
            }
            else if (humanRace == HumanRace.Southern){
                achternaamList = geefAchternaamListSouthernHuman();
            }
            else if (humanRace == HumanRace.Oriental){
                achternaamList = geefAchternaamListOrientalHuman();
            }
            else if (humanRace == HumanRace.Midlander_Anglo){
                achternaamList = geefAchternaamListMidlander_AngloHuman();
            }
            else {
                achternaamList = geefAchternaamListMidlander_GermanicHuman();
            }


        } else
            achternaamList = geefAchternaamListHalfling();
        return (String) randElementFromArray(achternaamList);
    }

    private static String[] geefAchternaamListHalfling() {
        return new String[]{

                "Appleblossom", "Bigheart", "Brightmoon", "Brushgather", "CherryCheeks", "Copperkettle",
                "Deephollow", "Elderberry", "Elderburry", "Fastfoot", "Fatrabbit", "Glenfellow", "Goldfound",
                "Goodbarrel", "Goodearth", "Greenbarrel", "Greenleaf", "High-hill", "Hilltopple", "Hogcollar", "Honeypot",
                "Jamjar", "Kettlewhistle", "Leagallow", "Littlefoot", "Nimblefingers", "Porridgepot", "Quickstep",
                "reedfellow", "Shadowquick", "Silvereyes", "Smoothhands", "Stonebridge", "Stoutbridge", "Stoutman",
                "Strongbones", "Sunmeadow", "Swifthistle", "Tallfellow", "Tealeaf", "Tenpenny", "Thistletop", "Thorngage",
                "Tosscobble", "Underbough", "Underfoot", "Warmweather", "Whispermouse", "Wildcloak", "Wildheart", "Wiseacre"

        };
    }
    private static String[] geefAchternaamListCelticHuman() {
        return new String[]{

                   "Airigh","Ais","Aislinn","Alsandair","Bagnall","Biadhtach","Binnse","Buaman",
                    "Boscach","Cadal","Caimpion","Ciarasach","Clanndioluin","Conndun","Corbaid","Corcait",
                    "Creag","De Beic","De Bluinnsiol","De Bhuinn","Easmonn","Easpog","Eichingeam","Eigear",
                    "Fada","Faedheach","Faghan","Fairsing","Fannt","Faoiteach","Feoirling","Fionglas","Fionn",
                    "Gaillidhe","Gaimlin","Gall","Gearlann","Giolcach","Hart","Hodnae","Hoiste","Huighleid",
                    "Iongram","Labhrant","Leabhailin","Liagh","Liath","Mac An Fhir","Mac An Tuile","Mac An Ultaigh",
                    "Mac Aodha","Mac Asmuint","Nioreis","Nuaman","Nuinnseann","Paghan","Ponns","Raghait","Rideal",
                    "Riodal","Sabhaois","Saghas","Saileastar","Sailigeir","Seac","Seadhach","Seafraidh","Seoghas",
                    "Talant","Tath","Tirial","Toc","Uachan","Uingil","Uinseann","Ultach"
        };
    }

    private static String[] geefAchternaamListNorthernHuman() {
        return new String[]{

                "Asgeir","Asketill","Asulf","Brynjolf","Arnulf","Hvalman","Gunnulf","Hastein","Havard",
                "Herulf","Holgata","Njall","Unfrid","Osmund","Osulf","Ragnulf","Thorvald","Thorstein",
                "Varangr","Vilulf","Yver"


        };
    }
    private static String[] geefAchternaamListSouthernHuman() {
        return new String[]{

                //List of Italian based https://en.wiktionary.org/wiki/Appendix:Italian_surnames
                "Abate","Adria","Agonstinelli","Baglioni","Baresi","Bucci","Canali","Ciccone",
                "Clemenza","Donini","Durante","Errani","Esposito","Evangelista","Fallaci",
                "Faugno","Ferri","Fiore","Gallo","Gianelli","Izanghi","Jilani","Loggia","Lubrano",
                "Mazza","Mele","Monaldo","Natale","Necci","Nicoletti","Opizzi","Oscuro","Pagnotto","Pirovano",
                "Quinto","Ragusa","Rizza","Rossi","Sagese","Scutari","Spada","Testa","Tocci","Tropea",

                //List of portuguese based https://www.momjunction.com/articles/portuguese-surnames-last-names-meanings_00521594/
                //"Alto","Avila","almada","Abreu","Barbas","Barboza"
                //decide later if I want to use these or just use the italian surnames with the roman names of the book

        };
    }

    private static String[] geefAchternaamListOrientalHuman() {
        return new String[]{

                "Abadi","Abboud","Almasi","Antar","Asker","Basara","Baz","Bahar","Boutros","Cham","Dagher","Guirguis",
                "Ganem","Haik","Hakimi","Isa","Kattan","Kouri","Maloof","Mifsud","Nader","Nahas","Naifeh","Nassar",
                "Rahal","Safar","Salib","Said","Tahan","Tannous","Toma","Tuma","Zogby","Qureshi","Harb"


        };
    }
    private static String[] geefAchternaamListFrankishHuman() {
        return new String[]{

                "Oriental1",
                "Oriental2",
                "Oriental3"

        };
    }

    private static String[] geefAchternaamListMidlander_AngloHuman() {
        return new String[]{

               "Addington","Adley","Ainsworth","Atherton","Barclay","Barlow","Becker","Blackwood",
                "Bircher","Blythe","Breeden","Cason","Chilcott","Clayden","Colby","Compton","Cromwell",
                "Darnall","Deighton","Dryden","Dudley","Earnshaw","Elton","Everleigh","Farley","Fossey","Fulton",
                "Gladstone","Graeme","Hadley","Hallewell","Hargrave","Huxley","Kenley","Kendall","Knapton",
                "Lancaster","Langdon","Leighton","Manley","Marleigh","Merton","Nibley","Northcott","Oakley",
                "Ogden","Pickering","Quinton","Ramsey","Redfield","Sherwood","Skelton","Smalton","Spaulding",
                "Tenley","Thornton","Thorpe","Trafford","Tyndal","Vance","Wakefield","Whitewall","Warwick","York",



// note , search for norman family names
        };
    }


    private static String[] geefAchternaamListMidlander_GermanicHuman() {
        return new String[]{

                "Ahlberg","Althaus","Assendorf","Bauer","Becke","Bergmann","Blumenthal",
                "Daube","Diefenbach","Drechsler","Dreier","Eckstein","Eichel","Esser","Faerber",
                "Feld","Fiedler","Forst","Freimann","Freudenberger","Gerber","Goldschmidt","Grünewald",
                "Hasenkamp","Hauptmann","Hoffmann","Holzknecht","Jaeger","Kaufmann","Kohler","Kurzmann",
                "Neumann","Pfeiffer","Schneider","Shuchardt","Schultz","Schwarz","Stern","Taube",
                "Vogel","Voss","Wagner","Walkenhorst","Winter"


        };
    }

    private static String[] geefAchternaamListElf() {
        return new String[]{

                "Aloro","Amakiir","Amastacia","Ariessus","Arnuanna","Berevan","Caerdonel",
                "Casilltenirra","Cithreth","Dalanthan","Eathalena","Erenaeth","Fasharash","Firahel",
                "floshem","Galanodel","Goltorah","Hanali","Holimion","Horineth","Lathelas","Liadon",
                "Meliamne","Mellerele","Mystraleth","Ostoroth","Queldrae","Qualanthri","Raethran","Rothenel",
                "Selverarun","Siannodel","Suithrasas","Sylvaranth","Sylvaneth","Tirvannel","Withrenn","Yaeldrin"


        };
    }

    private static String[] geefAchternaamListDwarf() {
        return new String[]{

                "Aranore", "Balderk", "Battlehammer", "Bigtoe", "Bloodkith", "Boffdan", "Brawnanvil", "Brazzik", "Broodfist",
                "Burrowfound", "Caebrek", "Daerdahk", "Dankil", "Daraln", "Deepdelver", "Durthane", "Eversharp", "Foamtankard",
                "Frostbeard", "Glanhig", "Goblinbane", "Goldfinder", "Gorunn", "Greybeard", "Hammerstone", "Helcral", "Holdern",
                "Ironfist", "Loderr", "lutgehr", "Morigak", "Orcfeller", "Orcfoe", "Rakankrak", "Ruby-eye", "Rummaheim",
                "Silveraxe", "Silverstone", "Steelfist", "Stoutale", "Strakeln", "Strongheart", "Thrarak", "Torevir", "Torunn",
                "Trollbleeder", "Trueanvil", "Trueblood", "Ungart"


        };
    }


    private static String genVoornaam(Gender gender, Race race) {
        String[] voornaamList;
        if (gender == Gender.MALE) {
            if (race == Race.DWARF) {
                voornaamList = geefVoornaamListMaleDwarf();
            } else if (race == Race.ELF) {
                voornaamList = geefVoornaamListMaleElf();
            } else if (race == Race.HUMAN) {
                if(humanRace == HumanRace.Celtic){
                    voornaamList = geefVoornaamListMaleCelticHuman();
                }
                else if (humanRace == HumanRace.Northern){
                    voornaamList = geefVoornaamListMaleNorthernHuman();
                }
                else if (humanRace == HumanRace.Southern){
                    voornaamList = geefVoornaamListMaleSouthernHuman();
                }
                else if (humanRace == HumanRace.Oriental){
                    voornaamList= geefVoornaamListMaleOrientalHuman();
                }
                else if (humanRace == HumanRace.Midlander_Anglo){
                    voornaamList = geefVoornaamListMaleAngloHuman();
                }
                else {
                    voornaamList = geefVoornaamListMaleGermanicHuman();
                }

            } else
                voornaamList = geefVoornaamListMaleHalfling();


        } else {
            if (race == Race.DWARF) {
                voornaamList = geefVoornaamListFemaleDwarf();
            } else if (race == Race.ELF) {
                voornaamList = geefVoornaamListFemaleElf();
            } else if (race == Race.HUMAN) {
                if(humanRace == HumanRace.Celtic){
                    voornaamList = geefVoornaamListFemaleCelticHuman();
                }
                else if (humanRace == HumanRace.Northern){
                    voornaamList = geefVoornaamListFemaleNorthernHuman();
                }
                else if (humanRace == HumanRace.Southern){
                    voornaamList = geefVoornaamListFemaleSouthernHuman();
                }
                else if (humanRace == HumanRace.Oriental){
                    voornaamList= geefVoornaamListFemaleOrientalHuman();
                }
                else if (humanRace == HumanRace.Midlander_Anglo){
                    voornaamList = geefVoornaamListFemaleAngloHuman();
                }
                else {
                    voornaamList = geefVoornaamListFemaleGermanicHuman();
                }

            } else
                voornaamList = geefVoornaamListFemaleHalfling();
        }
        return (String) randElementFromArray(voornaamList);
//Idea : if Human (within that if) getrandomId from subrace specific for each race ? and then another if for each value ?
    }

    private static String[] geefVoornaamListFemaleHalfling() {
        return new String[]{

                "Alaina","Andry","Anne","Bella","Blossom","Bree","Callie","Chenna","Cora","Della","Eida","Ellie",
                "Eran","Euphemia","Georgina","Gynnie","Harriet","Jasmine","Jillian","Jo","Kithri","Lavinia",
                "Lidda","Maegan","Marigold","Merla","Myra","Nedda","Nikki","Nora","Olivia","Paela","Pearl",
                "Pennie","Philomena","Rose","Saral","Seraphina","Shaena","Stacee","Tawna","Thea","Thrym","Tyna",
                "Vani","Verna","Wella","Willow"


        };

    }



    private static String[] geefVoornaamListFemaleCelticHuman() {
        return new String[]{

                "Aife","Aina","Alane","Ardena","Arienh","Beatha","Birgit","Briann","Caomh","Cara","Cinnia",
                "Cordelia","Deheune","Divone","Donia","Doreena","Elsha","Enid","Ethne","Evelina","Fianna",
                "Genevieve","Gilda","Gitta","Grania","Gwyndolin","Idelisa","Isolde","Keelin","Kennocha","Lavena",
                "Lesley","Linnette","Liona","Mabina","Marvina","Mavis","Mirna","Morgan","Muriel","Nareena",
                "Oriana","Regan","Ronat","Rowena","Selma","Ula","Venetia","Wynne","Erùona"


        };
    }

    private static String[] geefVoornaamListFemaleSouthernHuman() {
        return new String[]{

                "Aelia","Aemilia","Agrippina","Alba","Antonia","Aquila","Augusta","Aurelia","Balbina","Blandina",
                "Caelia","Camilla","Casia","Claudia","Cloelia","Domitia0","Drusa","Fabia","Fabricia","Fausta",
                "Flavia","Floriana","Fulvia","Germana","Glaucia","Gratiana","Hadriana","Hermina","Horatia",
                "Hortensia","Iovita","Iulia","Laelia","Laurentia","Livia","Longina","Lucilla","Lucretia",
                "Marcella","Marcia","Nona","Octavia","Paulina","Petronia","Porcia","Tacita","Tullia",
                "Verginia","Vita"


        };
    }
    private static String[] geefVoornaamListFemaleNorthernHuman() {
        return new String[]{

                "Alfhild","Arnbjorg","Ase","Aslog","Astrid","Auda","Audhid","Bergljot","Birghild","Bodil","Brenna",
                "Brynhild","Dagmar","Eerika","Eira","Gudrun","Gunborg","Gunhild","Gunvor","Helga","Hertha","Hilde",
                "Hillevi","Ingrid","Iona","Jorunn","Kari","Kenna","Magnhild","Nanna","Olga","Ragna","Ranveig",
                "Runa","Saga","Sigfrid","Signe","Sigrid","Sigrunn","Solveg","Svanhild","Thora","Torborg","Torunn",
                "Tove","Unn","Vigdis","Ylva","Yngvild"


        };
    }

    private static String[] geefVoornaamListFemaleOrientalHuman() {
        return new String[]{

                "Aaliyah","Aida","Akilah","Alia","Amina","Atefeh","Chaima","Dalia","Ehsan","ELham","Farah",
                "Fatemah","Gamila","Lesha","Inbar","Kamaria","Khadija","Layla","Lupe","Nabila","Nadine",
                "Naima","Najila","Najwa","Nakia","Nashwa","Nawra","Nuha","Nura","Qadira","Qistina","Rahima",
                "Rahima","Saadia","Sabah","Sada","Saffron","Sahar","Salma","Shatha","Tahira","Takisha","Thana",
                "Yadira","Zahra","Zaina","Zeinab"


        };
    }
    private static String[] geefVoornaamListFemaleFrankishHuman() {
        return new String[]{

                "Humangirl",


        };
    }

    private static String[] geefVoornaamListFemaleAngloHuman() {
        return new String[]{

                "Adeleide","Agatha","Agnes","Alice","Aline","Anne","Avelina","Beatrice","Cecily","Egelina","Eleanor",
                "Elizabeth","Ella","Eloise","Elysande","Emma","Emmeline","Ermina","Eva","Galiena","Geva","Giselle",
                "Griselda","Hadwisa","Helen","Herleva","Hugolina","Ida","Isabella","Jacelinde","Jane","Joan","Juliana",
                "Katherine","Margery","Mary","Matilda","Maynild","Millicent","Oriel","Rohesia","Rosalind","Rosamund",
                "Sarah","Sussanah","Sybil","Williamina","Yvonne"


        };
    }
    private static String[] geefVoornaamListFemaleGermanicHuman() {
        return new String[]{

                "Adelhayt","Althea","Agatha","Allet","Angnes","Anna","Apell","Applonia","Barbara","Brida","Brigita",
                "Cecilia","Clara","Cristina","Dorothea","Duretta","Ella","Els","Elsbeth","Erika","Enlein","Eva","Fela",
                "Fronicka","Genefe","Geras","Gertrudt","Helena","Irma","Jonata","Katerina","Kirsten","Lucia","Madalena",
                "Magdalen","Margret","Marlein","Martha","Otilia","Ottilg","Peternella","Sibilla","Thea","Ursel","Urselina"


        };
    }

    private static String[] geefVoornaamListFemaleElf() {
        return new String[]{

                "Adri", "Ahinar", "Althea", "Annastrianna", "Andraste", "Antinua", "Arara", "Baelitae", "Bethrynna", "Birel",
                "Caelynn", "Chaedi", "Claira", "Dara", "Drusilia", "Elema", "Enna", "Faral", "Felosial", "Hatae",
                "Ielenia", "Ilanis", "Jarsali", "Jelenneth", "Keyleth", "Laurielle" , "Leshanna" , "Lia","Lirielle",
                "Lorieth","Meriele", "Mialee","Muriel","Myathethil","Naivara","Quellenna","Sariel","Silaqui","Sunna",
                "Theirastra","Thiala","Traulam","Valanthes"

        };
    }

    private static String[] geefVoornaamListFemaleDwarf() {
        return new String[]{

                "Anbera", "Artin", "Audhild", "Balfira", "Barbena", "Bardryn", "Belhilde", "Dagnal", "Dariff", "Delre", "Diesa",
                "Eldeth", "Eridred", "Falkrunn", "Fallthra", "Finellen", "Gillydd", "Gunnloda", "Gurdis", "Helgret", "Helja",
                "Hlin", "Ilde", "Jarana", "Kathra", "Kilia", "Kristryd", "Liftrasa", "Marastyr", "Mardred", "Morana", "Nalead",
                "Nola", "Nurkara", "Oriff", "Ovina", "Riswynn", "Sannl", "Therlin", "Thodris", "Torbera", "Tordrid", "Torgga",
                "Valida", "Vistra", "Vonana", "Werydd", "Whurdred", "Yurgunna"


        };
    }


    private static String[] geefVoornaamListMaleHalfling() {
        return new String[]{

                "Alton","Ander","Bernie","Bobbin","Cade","Callus","Corrin","Dannad","Danniel","Eddie",
                "Egart","Eldon","Errich","Fildo","Finnan","Franklin","Garret","Garth","Gilbert","Gob",
                "Harol","Igor","Jasper","Keith","Kevin","Lazam","Lerry","Lindal","Lyle","Merric","Mican",
                "Milo","Morrin","Nebin","Nevil","Osborn","Ostran","Oswalt","Perrin","Poppy","Reed","Roscoe",
                "Sam","Shardon","Tye","Ulmo","Wellby","Wendel","Wenner","Wes"


        };

    }




    private static String[] geefVoornaamListMaleCelticHuman() {
        return new String[]{

                "Airell","Airic","Alan","Anghus","Aodh","Bardon","Bearacb","Bevyn","Boden",
                "Bran","Brasil","Bredon","Brian","Bricriu","Bryant","Cadman","Caradoc","Cedric",
                "Conalt","Conchobar","Condon","Darcy","Devin","Dillion","Donaghy","Donall","Duer",
                "Eghan","Ewyn","Ferghus","Galvyn","Gildas","Guy","Harvey","Iden","Irven","Karney",
                "Kayne","Kelvyn","Kennoch","Kunsgnos","Leigh","Maccus","Moryn","Neale","Owyn",
                "Pryderi","Reaghan","Taliesin","Tiernay","Turi"


        };
    }

    private static String[] geefVoornaamListMaleSouthernHuman() {
        return new String[]{

                "Aelius","Aetius","Agrippa","Albanus","Albus","Antonius","Appius","Aquilinus","Atilus",
                "Augustus","Aurelius","Avitus","Balbus","BLandus","Blasius","Brutus","Caelius","Caius","Casian",
                "Cassius","Cato","Celsus","Claudius","Cloelius","Cnaeus","Crispus","Cpyrianus","Diocletianus",
                "Egnatius","Ennius","Fabricius","Faustus","Gaius","Germanus","Gnaeus","Horatius","Iovianus",
                "Julius","Lucilius","Manius","Marcus","Marius","Maximus","Octavius","Paulus","Quintilian",
                "Servius","Tacitus","Varius"


        };
    }
    private static String[] geefVoornaamListMaleNorthernHuman() {
        return new String[]{

                "Agni","Alaric","Arnvindr","Arvid","Asger","Asmund","Bjarte","Bjorg","Bjorn","Brandr","Brandt","Brynjar",
                "Calder","Colborn","Cuyler","Egil","Einar","Eric","Erland","Fiske","Folkvar","Fritjof","Frode","Geir",
                "Halvar","Hemming","Hjalmar","Hjortr","Ingimarr","Ivar","Knud","Leif","Liufr","Manning","Oddr","Olin",
                "Ormr","Ove","Rannulfr","Sigurd","Skari","Snorri","Sten","Sven","Trygve","Ulf","Vali","Vidar"


        };
    }

    private static String[] geefVoornaamListMaleOrientalHuman() {
        return new String[]{

                "Abbad","Abdul","Achmed","Akeem","Alif","Amir","Asim","Bashir","Bassam","Fahim","Farid",
                "Farouk","Fayez","Fayyaad","Fazil","Hakim","Halil","Hamid","Hazim","Heydar","Hussein","Jabari",
                "Jafar","Jahid","Jamal","Kalim","Karim","Kazim","Khadim","Khalid","Mahmud","Mansour","Musharraf",
                "Mustafa","Nadir","Nazim","Omar","Qadir","Dusay","Rafiq","Rakim","Rashad","Rauf","Saladin","Sami",
                "Samir","Talib","Tamir","Tariq","Yazid"


        };
    }
    private static String[] geefVoornaamListMaleFrankishHuman() {
        return new String[]{

                "Humanguy",


        };
    }

    private static String[] geefVoornaamListMaleAngloHuman() {
        return new String[]{

                "Adam","Adelard","Aldous","Anselm","Arnold","Bernard","Bertram","Charles","Clerebold","Conrad",
                "Diggory","Diegbald","Everard","Frederick","Geoffrey","Gerald","Gilbert","Godfrey","Gunter","Guy",
                "Hentry","Heward","Hubert","Hugh","Jocelyn","John","Lance","Manfred","Miles","Nicholas","Norman",
                "Odo","Percival","Peter","Ralf","Randal","Raymond","Reynard","Richard","Robert","Roger","Roland",
                "Rolf","Simon","Theobald","Theodoric","Thomas","Timm","William","Wymar"


        };
    }


    private static String[] geefVoornaamListMaleGermanicHuman() {
        return new String[]{

                "Albrecht","Allexander","Baltasar","Benedick","Berhart","Caspar","Clas","Cristin","Cristoff","Dieterich",
                "Engelhart","Erhart","Felix","Frantz","Fritz","Gerhart","Gotleib","Hans","Hartmann","Herman",
                "Jacob","Jorg","Karll","Kilian","Linhart","Lorentz","Ludwig","Marx","Melchor","Mertin","Michel",
                "Moritz","Osswald","Ott","Peter","Rudolff","Ruprecht","Sebastian","Sigmund","Steffan","Symon",
                "Thoman","Ulrich","Vallentin","Wendel","Wilhelm","Wolff","Wolfgang"


        };
    }

    private static String[] geefVoornaamListMaleElf() {
        return new String[]{

                "Adran","Aelar","Aerdeth","Ahvain","Aramil","Arannis","Aust","Azaki","Beiro","Berrian","Caeldrim",
                "Carric","Dayereth","Draeli","Efferil","Eiravel","Enialis","Erdan","Erevan","Fivin","Galinndan",
                "Gennal","Hadarai","Halimath","Heian","Himo","Immeral","Ivellios","Korfel","Lamlis","Laucian",
                "Lucan","Mindartis","Naal","Paelias","Periath","Quarion","Riardon","Rolen","Soveliss","Suhnae",
                "Thamior","Tharivol","Theren","Theriatis","Thervan","Uthemar","Vanuath","Varis"


        };
    }

    private static String[] geefVoornaamListMaleDwarf() {
        return new String[]{

                "Adrik","Alberich","Baern","Barendd","Beloril","Brottor","Dain","Dalgal","Darrak","Delg",
                "Duergath","Dworic","Eberk","Einkil","Elaim","Erias","Fallond","Fargrim","Gardain","Gilthur",
                "Gimgen","Gimurt","Harbek","Kildrak","Kilvar","Morgran","Morgrim","Morkral","Nalral","Nordak",
                "Nuruval","Oloric","Olunt","Orsik","Orskar","Rangrim","Reirak","Rurik","Taklinn","Thoradin",
                "Thorin","Thradal","Tordek","Traubon","Travok","Ulfgar","Uraim","Veit","Vonnbin","Vondal",
                "Whurbin"


        };
    }

    private static Gender genGender() {
        return (Gender) randElementFromArray(Gender.values());
    }

    private static Object randElementFromArray(Object[] array) {
        return array[randIdx(array)];
    }

    private static int randIdx(Object[] array) {
        int arrayLength = array.length;
        return RAND_NUM_GEN.nextInt(arrayLength);
    }


}
