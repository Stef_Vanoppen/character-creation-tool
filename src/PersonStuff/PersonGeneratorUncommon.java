package PersonStuff;

import enums.Gender;
import enums.Profession;
import enums.RaceUncommon;

import java.util.Collection;
import java.util.HashSet;
import java.util.Random;


public class PersonGeneratorUncommon {
    private final static Random RAND_NUM_GEN = new Random();

    public static Person generate() {
        Gender gender = genGender();
        RaceUncommon raceUncommon = genRace();
        Profession profession = genProfessions();
        String voornaam = genVoornaam(gender , raceUncommon);
        String achternaam = genAchternaam(raceUncommon); //verwijzing naar de lists
        return new Person(voornaam, achternaam, gender,raceUncommon, profession);
    }

    private static Profession genProfessions() {
        return (Profession)
                randElementFromArray(Profession.values());
    }


    private static RaceUncommon genRace() { return (RaceUncommon) randElementFromArray(RaceUncommon.values());
    }



    public static Collection<Person> generatePeople(int amount) {
        Collection<Person> people = new HashSet<>(); //swapped to hashset
        for (int i = 0; i < amount; i++) {
            people.add(generate());
        }
        return people;
    }




    private static String genAchternaam(RaceUncommon raceUncommon) {
        String[] achternaamList;
        if (raceUncommon == RaceUncommon.DRAGONBORN){
            achternaamList = geefAchternaamListDragonborn();
        }
        else if (raceUncommon == RaceUncommon.GNOME){
            achternaamList = geefAchternaamListGnome();
        }
        else if (raceUncommon == RaceUncommon.HALF_ELF){
            achternaamList = geefAchternaamListHalfElf();
        }

        else if (raceUncommon == RaceUncommon.HALF_ORC){
            achternaamList = geefAchternaamListHalfOrc();
        }
        else
            achternaamList = geefAchternaamListTiefling();
        return (String) randElementFromArray(achternaamList);
    }

    private static String[] geefAchternaamListDragonborn() {
        return new String[]{

                "lizard1",
                "lizard2"


        };
    }

    private static String[] geefAchternaamListGnome() {
        return new String[]{

                "Gnome3",
                "Gnome2",
                "gnome1"

        };
    }

    private static String[] geefAchternaamListHalfElf() {
        return new String[]{

                "halfelf1",
                "halfelf2"


        };


    }

    private static String[] geefAchternaamListHalfOrc() {
        return new String[]{

                "halforc2",
                "halforc1"


        };
    }

    private static String[] geefAchternaamListTiefling() {
        return new String[]{

                "Tieflastname",
                "tieflastname2"


        };
    }


// CHANGE THE NAME LISTS ON THIS ONE SQUIRREL , DO NOT FORGET OR YOU WILL BRING DISHONOUR UPON THE CLAN


    private static String genVoornaam(Gender gender, RaceUncommon race) {
        String[] voornaamList;
        if (gender == Gender.MALE) {
            if (race == RaceUncommon.DRAGONBORN){
                voornaamList = geefVoornaamListMaleDragonborn();
            }
            else if (race == RaceUncommon.GNOME){
                voornaamList = geefVoornaamListMaleGnome();
            }
            else if (race == RaceUncommon.HALF_ELF){
                voornaamList = geefVoornaamListMaleHalfElf();
            }
            else if (race == RaceUncommon.HALF_ORC){
                voornaamList = geefVoornaamListMaleHalfOrc();
            }
            else
                voornaamList = geefVoornaamListMaleTiefling();






        } else {
            if (race == RaceUncommon.DRAGONBORN){
                voornaamList = geefVoornaamListFemaleDragonborn();
            }
            else if (race == RaceUncommon.GNOME){
                voornaamList = geefVoornaamListFemaleGnome();
            }
            else if (race == RaceUncommon.HALF_ELF){
                voornaamList = geefVoornaamListFemaleHalfElf();
            }

            else if (race == RaceUncommon.HALF_ORC){
                voornaamList = geefVoornaamListFemaleHalfOrc();
            }
            else
                voornaamList = geefVoornaamListFemaleTiefling();
        }
        return (String) randElementFromArray(voornaamList);

    }


    private static String[] geefVoornaamListFemaleTiefling() {
        return new String[]{

                "SexyGoatLady",


        };
    }

    private static String[] geefVoornaamListFemaleHalfOrc() {
        return new String[]{

                "PartiallyGreen",


        };

    }

    private static String[] geefVoornaamListFemaleHalfElf() {
        return new String[]{

                "ForgotTheOtherHalf",


        };
    }

    private static String[] geefVoornaamListFemaleGnome() {
        return new String[]{

                "CompactIntelligence",


        };
    }

    private static String[] geefVoornaamListFemaleDragonborn() {
        return new String[]{

                "Lizardpeoplegirl",


        };
    }


    private static String[] geefVoornaamListMaleTiefling() {
        return new String[]{

                "Chupacabra",


        };
    }


    private static String[] geefVoornaamListMaleHalfOrc() {
        return new String[]{

                "PartiallyOrc",


        };

    }

    private static String[] geefVoornaamListMaleHalfElf() {
        return new String[]{

                "SortaElf",


        };
    }

    private static String[] geefVoornaamListMaleGnome() {
        return new String[]{

                "Gnome",


        };
    }

    private static String[] geefVoornaamListMaleDragonborn() {
        return new String[]{

                "Dragonborn",


        };
    }

    private static Gender genGender() {
        return (Gender) randElementFromArray(Gender.values());
    }

    private static Object randElementFromArray(Object[] array) {
        return array[randIdx(array)];
    }

    private static int randIdx(Object[] array) {
        int arrayLength = array.length;
        return RAND_NUM_GEN.nextInt(arrayLength);
    }



}



