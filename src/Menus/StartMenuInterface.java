package Menus;

public interface StartMenuInterface {


    void start();

    void newCharacter();

    void newCharactersMultiple();

   // void load();

   // void save();

    void quit();

    void options(String helpSpecific, String Input);



}
