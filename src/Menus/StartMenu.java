package Menus;


import Utilities.KeyboardUtility;
import Utilities.MenuUtility;

import java.sql.SQLOutput;

public class StartMenu implements StartMenuInterface {

    private boolean keepGoing;

    @Override
    public void start() {

        System.out.println(MenuUtility.thickLine());
        System.out.println(MenuUtility.center("Sleep deprived critters"));
        System.out.println(MenuUtility.thickLine());
        System.out.println(MenuUtility.center("D&D Character generation tool"));
        System.out.println(MenuUtility.thinLine());


        System.out.println("New character\n" + "Quit\n");
        keepGoing = true;
        while (keepGoing) {
            options("", KeyboardUtility.ask(">"));

        }

    }

    @Override
    public void newCharacter() {
        System.out.println("this will soon start creating our chars");

        // TODO: 10/04/2021

        //reference towards creating our lovely characters one by one
        //We want to have a series of choices where we first choose our race then gender then profession
        //Or have an option that goes full random but retains previous choices
    }

    @Override
    public void newCharactersMultiple() {

    }

    @Override
    public void quit() {
        System.out.println("Generator shutting down");
    }

    @Override
    public void options(String helpSpecific, String input) {
        switch (input) {

            case "New character":
                newCharacter();
                break;
             case "Create a list of new characters":
                newCharactersMultiple();
                break;
            // case "load":
            //     load();
            //     break;
            case "Quit":
                quit();
                keepGoing = false;
                break;
            default:
                System.out.println("Please write the instruction correctly");
                break;
        }
    }

}
