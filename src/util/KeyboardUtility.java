package util;

import java.util.Scanner;

public final class KeyboardUtility {
    private static final String INVALID_MSG = "Invalid input! Please try again...";
    public static final Scanner KEYBOARD = new Scanner(System.in);

    public static int askForInt(String message) {
        while (true) {
            String input = ask(message);
            try {
                return Integer.parseInt(input);
            } catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }

    public static boolean askYOrN(String message) {
        while (true) {
            String input = ask(message + "(y/n)");
            char firstLetter = '0';
            try {
                firstLetter = input.toLowerCase().charAt(0);
            } catch (Exception e) {
                System.out.println(INVALID_MSG);
                continue;
            }
            switch (firstLetter) {
                case 'y':
                    return true;
                case 'n':
                    return false;
                default:
                    break;
            }
        }
    }

    public static String ask(String message) {
        System.out.println(message);
        return KEYBOARD.nextLine();
    }

    public static double askForDouble(String message) {
        while (true) {
            String input = ask(message);
            try {
                return Double.parseDouble(input);
            } catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }

    public static int askForChoice(String message, String[] options) {
        System.out.println(message);
        return askForChoice(options);
    }

    public static int askForChoice(String[] options) {
        while (true) {
            for (int i = 0; i < options.length; i++) {
                System.out.printf("%d. %s%n", i + 1, options[i]);
            }
            int chosenIdx = askForInt(String.format("Enter your choice (1-%d):", options.length)) - 1;
            if (chosenIdx < 0 || chosenIdx >= options.length){
                System.out.println(INVALID_MSG);
                System.out.println("Please enter a choice in the valid range");
            } else {
                return chosenIdx;
            }
        }
    }

    public static <T extends Enum> T askForChoice(T[] options){
        String[] enumAsString = new String[options.length];
        for (int i = 0; i < enumAsString.length; i++) {
            enumAsString[i] = TextUtil.snakeCaseToTitleCase(options[i].name());
        }
        return options[askForChoice(enumAsString)];
    }

    public static <T extends Enum> T askForChoice(String message, T[] options){
        System.out.println(message);
        return askForChoice(options);
    }

    public static String askForEmail(String message) {
        while (true) {
            try {
                String input = ask(message);
                final String EMAIL_PATTERN = "^[A-Za-z0-9+_.-]+@(.+)$";
                if (input.matches(EMAIL_PATTERN)) {
                    return input;
                } else {
                    throw new IllegalArgumentException("Email must be of pattern: user@domain.com");
                }
            } catch (IllegalArgumentException iee) {
                System.out.println(INVALID_MSG);
                System.out.println(iee.getMessage());
            }
        }
    }
}
