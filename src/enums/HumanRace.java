package enums;

public enum HumanRace {

    Celtic,
    Southern,
    Northern,
    Oriental,
    Midlander_Anglo,
    Midlander_Germanic,

}
